package Testes;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

import org.junit.Before;
import org.junit.Test;

import cadastrodisciplina2.Aluno;

public class AlunoTest {
	
	Aluno umAluno;
	
    @Before
    public void setUp() throws Exception {
     umAluno = new Aluno();
    }

    @Test
    public void testSetNome() {
    umAluno.setNome("Teste");
    assertEquals("Teste", umAluno.getNome());
    }
    
    @Test
    public void testSetMatricula() {
    umAluno.setMatricula("Teste");
    assertEquals("Teste", umAluno.getMatricula());
    }

}
