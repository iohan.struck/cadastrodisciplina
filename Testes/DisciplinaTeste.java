package Testes;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import cadastrodisciplina2.Aluno;
import cadastrodisciplina2.Disciplina;

public class DisciplinaTeste {

	Disciplina  umaDisciplina;
	

    @Before
    public void setUp() throws Exception {
     umaDisciplina = new Disciplina();
    }

    @Test
    public void testSetCodigo() {
    umaDisciplina.setCodigo("Teste");
    assertEquals("Teste", umaDisciplina.getCodigo());
    }
    
    @Test
    public void testSetNomeDisciplina() {
    umaDisciplina.setNomeDisciplina("Teste");
    assertEquals("Teste", umaDisciplina.getNomeDisciplina());
    }

}
